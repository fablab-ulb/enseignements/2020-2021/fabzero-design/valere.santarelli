Salut, moi c’est **Valère Santarelli**, j’ai 25 ans et je suis étudiant en **Master à la faculté d’architecture de la Cambre Horta à Bruxelles.**
Bienvenue sur Gitlab, je vous présente mon profil qui a pour but de vous exprimer mon parcours général et ma personnalité, plus particulièrement mon choix de l’option design et architecture et ainsi l’objet d’art que j’ai choisi. 
Les objectifs d’ensemble du cours seront d’expérimenter la matière et d’utiliser les technologies mises a notre disposition tout en effectuant une démarche d’archivage de nos expériences et de notre avancement sur cette plateforme Git, cela afin de réaliser un travail final de processus de création d’un objet. 


## About me

![](images/val-vs.jpg)  


## My background

Je suis Français, née à Ajaccio en Corse où j’y ai vécu jusqu’à mes 18 ans, âge auquel j’ai obtenu mon diplôme du Baccalauréat. J’ai à ce moment-là décidé de partir vers de nouveaux horizons, j’ai voyagé entre l’Angleterre et l’Italie pendant plusieurs mois, des expériences enrichissantes qui m’ont permis de découvrir de nouvelles cultures et langues. Voulant éveiller mon côté créatif, je me suis entre temps tourné vers des études d’architecture en intégrant La Cambre Horta à Bruxelles, ces études qui m’ont menée aujourd’hui en première année de Master. 
Pour compléter ma personnalité, j’aime l’aventure, bouger et sortir, je suis assez manuel et j’aime créer, construire et réparer tout ce qui m’entoure.
J’ai pour cette année 2020-2021, j’ai choisi l’atelier **''Architecture construite''**.
## Passions
Mes hobbies sont la musique et le dessin, je suis un adepte de randonnées sportives et de foot. J’aime aussi la déco intérieur, l’aménagement des espaces et le mobilier contemporain.
## Previous work

En 2019 j’ai développé un projet de **mobilier urbain**, c’était une série de bancs en bois, modulables et assemblables entre eux de plusieurs manière différente. Un travail important a été effectué sur les nœuds d’assemblage composé en ''Tenon-Mortaise’’. J’ai modélisé mon projet sur sketchup puis j’ai effectué des découpes de pièces de bois en utilisant la machine **C.N.C** pour mes maquettes à l’échelle 1.200.
![](images/renduetmaquette-2019.jpg)
![](images/rendus.jpg)
### Mon objet

Le **porte-revues** nommé **Portariviste** pour Kartell a été conçu par l'architecte **Giotto Stoppino** en **1972**. Conçu en **ABS** il est composé de six compartiments assurant sa structure, ceux-ci sont solidaires entre eux et sont disposés en cascade. Ainsi, ils servent aussi bien de maintiens structurels de l’ensemble, et de supports verticaux pour des magazines, revus et journaux.

On y trouve une poignée circulaire au centre pour le déplacer et des percements en fond pour le gain de matière et le nettoyage. 
C’est un objet qui se caractérise par un aspect très **minimaliste** et assez **intemporel**, avec un design graphique et coloré assuré par des courbures saillantes. Cet objet lie aussi bien **l’esthétisme et la fonctionnalité.** 

Sa ligne futuriste rappelle les projets architecturaux les plus modernes des années septante.

![](images/b.jpg) 
![](images/c.jpg)
![](images/a.jpg)

### Raisons de ce choix

Ce qui me plaît dans cet objet, c’est sa géométrie et son assemblage de forme mêlant **simplicité et complexité**. Il éveille ma curiosité et je me pose des questions sur sa forme et son équilibre. De plus, c’est un objet qui m’avait déjà marqué auparavant, je l’avais vu dans des boutiques et dans des intérieurs. Je fais souvent appel à ma mémoire implicite lorsqu’il s’agit de choisir une forme ou un objet parmi d’autres. 

![](images/porta.jpg)

_**PORTE-REVUS/PORTARIVISTE**_

-Giotto Stoppino, 1972-
_Kartell, Italie_

kartell.com 


